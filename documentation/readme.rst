============
  XPM Draw
============


**XPM Draw** is a command line application for parsing XPM image files and
drawing them in the terminal itself.

This program is free software, thus you are welcome to use, change and
redistribute it.  It is licensed under the terms of the GNU GPLv3+ (GNU
General Public License, either version 3 or higher).


Requirements
============

The `getopt_long` function is required, for parsing command line options.  It
is known to be included in the GNU/Linux, FreeBSD and NetBSD operating
systems.

Apart from that, the almost only “special” thing you need is an ANSI/VT100
terminal or emulator; and practically all modern Unix systems, such as
GNU/Linux, include one.  This is needed for outputting colors.


Links
=====

* XPM 3 specification: http://www.xfree86.org/current/xpm.pdf
