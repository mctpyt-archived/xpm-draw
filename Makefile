TARGET         = xpmdraw
CFLAGS         = -fdiagnostics-color=auto -Wall -DFANCY
OBJECTS        = io.o main.o util.o xpm.o xpm_color.o xpm_colormode.o \
                 xpm_draw.o
VERSION_NUMBER = git-$(shell git show --format=%h -s)

.PHONY: all clean

all: $(TARGET)

clean:
	$(RM) $(TARGET) $(OBJECTS) config.h

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

fancy.h: terminalcode.h

io.o: source/io.c source/io.h source/fancy.h
	$(CC) -c $(CFLAGS) -o $@ $<

main.o: source/main.c source/fancy.h source/io.h source/xpm.h \
        source/xpm_colormode.h source/xpm_draw.h config.h
	$(CC) -c $(CFLAGS) -I. -o $@ $<

util.o: source/util.c source/util.h source/fancy.h
	$(CC) -c $(CFLAGS) -o $@ $<

xpm.o: source/xpm.c source/xpm.h source/io.h source/util.h \
       source/xpm_color.h source/xpm_colormode.h source/xpm_draw.h \
       source/xpm_types.h
	$(CC) -c $(CFLAGS) -o $@ $<

xpm_color.o: source/xpm_color.c source/xpm_color.h source/xpm_colormode.h
	$(CC) -c $(CFLAGS) -o $@ $<

xpm_colormode.o: source/xpm_colormode.c source/xpm_colormode.h
	$(CC) -c $(CFLAGS) -o $@ $<

xpm_draw.o: source/xpm_draw.c source/xpm_draw.h source/xpm.h \
            source/xpm_colormode.h source/xpm_types.h
	$(CC) -c $(CFLAGS) -o $@ $<

config.h: source/config.h.tpl
	sed 's/%%VERSION_NUMBER%%/$(VERSION_NUMBER)/g' $< >$@
