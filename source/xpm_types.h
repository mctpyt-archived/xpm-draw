/***************************************************************************
 * XPM private types.                                                      *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_XPM_TYPES_H
#define XPMDRAW_XPM_TYPES_H


#include "xpm_color.h"
#include <stdbool.h>


/**
 * A point, represented as an index in an array of colors.
 *
 * The size of this type determines the color depth, and also an important
 * part of the memory consumption (although depending on each case, an array
 * of colors may consume much more than an array of points)
 */
typedef int xpm_point_t;

/**
 * XPM information structure.
 *
 * All information about an XPM file extracted by parsing.  This is the main
 * XPM structure, all others are used by this one.
 */
typedef struct xpm {
    unsigned     width;
    unsigned     height;
    unsigned     color_count;
    unsigned     characters_per_point;
    unsigned     extension_count;
    unsigned     x_hotspot;
    unsigned     y_hotspot;
    bool         has_hotspot;
    xpm_color_t *colors;
    xpm_point_t *points;
} xpm_t;


#endif
