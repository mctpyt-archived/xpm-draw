/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "util.h"
#include "fancy.h"
#include "io.h"
#include <stdlib.h>


/***************************************************************************
 * Macros                                                                  *
 ***************************************************************************/

/** String for outputting an ellipsis. */
#define ELLIPSIS  FANCY_BLUE             "..." FANCY_RESET

/** String for outputting an up-arrow. */
#define ARROW     FANCY_GREEN FANCY_BOLD "^"   FANCY_RESET


/***************************************************************************
 * Public functions                                                        *
 ***************************************************************************/

/* Only works for character encoding where the digits `0` to `9`, the
   uppercase letters `A` to `F` and the lowercase letters `a` to `f` are
   mapped to contiguous and ordered ranges.  One such character encoding is
   ASCII. */
bool util_hexdigit_to_int(char      hexdigit,
                          unsigned *destination)
{
    if (hexdigit >= '0' && hexdigit <= '9')
        *destination = hexdigit - '0';
    else if (hexdigit >= 'A' && hexdigit <= 'F')
        *destination = hexdigit - 'A';
    else if (hexdigit >= 'a' && hexdigit <= 'f')
        *destination = hexdigit - 'a';
    else
        return false;
    return true;
}

void util_get_cursor_info(const char         *string,
                          size_t              cursor,
                          util_cursor_info_t *info_destination)
{
    size_t i, line, column, line_beginning, line_end;

    /* Calculate line and column numbers and line limits. */
    for (i = line_beginning = 0, line = 1; i < cursor; i++)
        if (string[i] == '\n') {
            line_beginning = i + 1;
            line++;
        }
    column = cursor - line_beginning + 1;
    for (; string[i] != '\n' && string[i] != '\0'; i++);
    line_end = i;

    info_destination->string         = string;
    info_destination->cursor         = cursor;
    info_destination->line           = line;
    info_destination->column         = column;
    info_destination->line_beginning = line_beginning;
    info_destination->line_end       = line_end;
}

void util_draw_cursor_info(FILE                     *stream,
                           const util_cursor_info_t *info)
{
    size_t i, start, end;

    /* Show cursor position intuitively.  Truncate output if too long. */

    fprintf(stream, IO_INDENTATION);

    /* Truncate at the beginning if needed. */
    start = info->cursor - info->cursor % 70;
    if (start > info->line_beginning)
        fprintf(stream, "%s", ELLIPSIS);
    else
        start = info->line_beginning;

    /* Truncate at the end if needed. */
    end = start + 70;
    if (end < info->line_end) {
        fwrite(&info->string[start], 1, end - start, stream);
        fprintf(stream, "%s", ELLIPSIS);
    } else
        fwrite(&info->string[start], 1, info->line_end - start, stream);

    fprintf(stream, "\n" IO_INDENTATION);
    for (i = start; i < info->cursor; i++)
        fprintf(stream, " ");
    if (start > info->line_beginning)
        fprintf(stream, "   ");
    fprintf(stream, ARROW "\n");
}

void *util_malloc(size_t size)
{
    void *result;

    result = malloc(size);
    if (result == NULL) {
        io_print_error("could not allocate memory");
        abort();
    } else
        return result;
}

void *util_calloc(size_t element_count,
                  size_t element_size)
{
    void *result;

    result = calloc(element_count, element_size);
    if (result == NULL) {
        io_print_error("could not allocate memory");
        abort();
    } else
        return result;
}

void *util_realloc(void  *pointer,
                   size_t size)
{
    void *result;

    result = realloc(pointer, size);
    if (result == NULL) {
        io_print_error("could not reallocate memory");
        abort();
    } else
        return result;
}
