/***************************************************************************
 * Fancy effects on terminal output.                                       *
 *                                                                         *
 * Effects include colored, bold and italic text.  Their usage depends on  *
 * the definition of the `FANCY` macro: only if it is defined, text        *
 * effects are used; otherwise text is left undecorated.                   *
 *                                                                         *
 * See also `terminalcode.h`.                                              *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_FANCY_H
#define XPMDRAW_FANCY_H


#include "terminalcode.h"


#ifdef FANCY
#define FANCY_RESET   TERMINALCODE_RESET
#define FANCY_BOLD    TERMINALCODE_BOLD
#define FANCY_ITALIC  TERMINALCODE_ITALIC
#define FANCY_RED     TERMINALCODE_FG_RED
#define FANCY_GREEN   TERMINALCODE_FG_GREEN
#define FANCY_BLUE    TERMINALCODE_FG_BLUE
#else
#define FANCY_RESET
#define FANCY_BOLD
#define FANCY_ITALIC
#define FANCY_RED
#define FANCY_GREEN
#define FANCY_BLUE
#endif


#endif
