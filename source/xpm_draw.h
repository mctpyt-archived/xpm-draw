/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_XPM_DRAW_H
#define XPMDRAW_XPM_DRAW_H


#include "xpm.h"


/**
 * Draw in terminal, using the terminal color codes specified in
 * `terminalcode.h`, the image described in an XPM information structure.
 *
 * The parameter `data` is an XPM information structure to use as input.  The
 * parameter `mode` is an XPM color mode to select colors for.
 */
void xpm_draw(xpm_t          *data,
              xpm_colormode_t mode);


#endif
