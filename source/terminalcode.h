/***************************************************************************
 * Terminal codes for visual effects on text.                              *
 *                                                                         *
 * This employs ANSI/VT100 terminal escape codes.  For information about   *
 * these, see <https://en.wikipedia.org/wiki/ANSI_escape_code>.            *
 *                                                                         *
 * See also `fancy.h`.                                                     *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_TERMINALCODE_H
#define XPMDRAW_TERMINALCODE_H


#define TERMINALCODE_RESET       "\033[0m"
#define TERMINALCODE_BOLD        "\033[1m"
#define TERMINALCODE_ITALIC      "\033[3m"

#define TERMINALCODE_FG_BLACK    "\033[30m"
#define TERMINALCODE_FG_RED      "\033[31m"
#define TERMINALCODE_FG_GREEN    "\033[32m"
#define TERMINALCODE_FG_YELLOW   "\033[33m"
#define TERMINALCODE_FG_BLUE     "\033[34m"
#define TERMINALCODE_FG_MAGENTA  "\033[35m"
#define TERMINALCODE_FG_CYAN     "\033[36m"
#define TERMINALCODE_FG_WHITE    "\033[37m"

#define TERMINALCODE_BG_BLACK    "\033[40m"
#define TERMINALCODE_BG_RED      "\033[41m"
#define TERMINALCODE_BG_GREEN    "\033[42m"
#define TERMINALCODE_BG_YELLOW   "\033[43m"
#define TERMINALCODE_BG_BLUE     "\033[44m"
#define TERMINALCODE_BG_MAGENTA  "\033[45m"
#define TERMINALCODE_BG_CYAN     "\033[46m"
#define TERMINALCODE_BG_WHITE    "\033[47m"


#endif
