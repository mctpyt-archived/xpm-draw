/***************************************************************************
 * XPM color modes.                                                        *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_XPM_COLORMODE_H
#define XPMDRAW_XPM_COLORMODE_H


#define XPM_COLORMODE_COUNT  5

/**
 * XPM color mode identifier.
 */
typedef enum xpm_colormode {
    XPM_COLORMODE_M = 0,
    XPM_COLORMODE_G4,
    XPM_COLORMODE_G,
    XPM_COLORMODE_C,
    XPM_COLORMODE_S,
    XPM_COLORMODE_AUTODETECT,
    XPM_COLORMODE_INVALID
} xpm_colormode_t;


xpm_colormode_t xpm_colormode_from_string(const char *string);

const char *xpm_colormode_to_string(xpm_colormode_t mode);


#endif
