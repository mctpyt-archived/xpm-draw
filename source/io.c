/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "io.h"
#include "fancy.h"
#include "util.h"
#include <stdlib.h>


/***************************************************************************
 * Macros                                                                  *
 ***************************************************************************/

#define BUFFER_CHUNK_SIZE    1024
#define ERROR_PREFIX         FANCY_RED FANCY_BOLD "Error: " FANCY_RESET
#define ERROR_COORDS_PREFIX  "%s:%u:%u: "


/***************************************************************************
 * Types                                                                   *
 ***************************************************************************/

typedef struct io_error_coords {
    const char *file_name;
    unsigned    line;
    unsigned    column;
} io_error_coords_t;


/***************************************************************************
 * Data                                                                    *
 ***************************************************************************/

static io_error_coords_t coords;


/***************************************************************************
 * Public functions                                                        *
 ***************************************************************************/

char *io_read_whole(FILE *stream)
{
    char    *buffer;
    unsigned i, n;

    buffer = util_malloc(BUFFER_CHUNK_SIZE);
    for (i = 0, n = 2;; n++) {
        i += fread(&buffer[i], 1, BUFFER_CHUNK_SIZE - 1, stream);
        if (feof(stream))
            break;
        buffer = util_realloc(buffer, n * BUFFER_CHUNK_SIZE);
    }
    buffer[i] = '\0';

    return buffer;
}

void io_set_error_coords(const char *file_name,
                         unsigned    line,
                         unsigned    column)
{
    coords.file_name = file_name == NULL ? "<stdin>" : file_name;
    coords.line      = line;
    coords.column    = column;
}

void io_print_error(const char *format,
                    ...)
{
    va_list arg_list;

    va_start(arg_list, format);
    io_print_error_v(format, arg_list);
    va_end(arg_list);
}

void io_print_error_v(const char *format,
                      va_list     arg_list)
{
    fprintf(stderr, ERROR_PREFIX);
    if (coords.file_name != NULL) {
        /* Print error coordinates and then clear them. */
        fprintf(stderr, ERROR_COORDS_PREFIX,
                coords.file_name, coords.line, coords.column);
        coords.file_name = NULL;
    }
    vfprintf(stderr, format, arg_list);
    fprintf(stderr, "\n");
}
