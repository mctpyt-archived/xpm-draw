/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "xpm_color.h"


void xpm_color_print_value(FILE           *stream,
                           xpm_color_t     color,
                           xpm_colormode_t mode)
{
    const xpm_color_value_t value       = color.values[mode];
    const char             *mode_string = xpm_colormode_to_string(mode);

    switch (XPM_COLOR_GET_MEASURE(color, mode)) {
        case XPM_COLORMASK_NONE:
            break;
        case XPM_COLORMASK_RGB:
            fprintf(stream, "%s(#%0X%0X%0X)", mode_string,
                    value.rgb[0], value.rgb[1], value.rgb[2]);
            break;
        case XPM_COLORMASK_HSV:
            fprintf(stream, "%s(%%%0X%0X%0X)", mode_string,
                    value.hsv[0], value.hsv[1], value.hsv[2]);
            break;
        case XPM_COLORMASK_NAME:
            fprintf(stream, "%s(%s)", mode_string, value.name);
            break;
    }
}
