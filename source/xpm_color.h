/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_XPM_COLOR_H
#define XPMDRAW_XPM_COLOR_H


#include "xpm_colormode.h"
#include <stdio.h>


#define XPM_COLOR_CLEAR(color)              ((color).mask = 0)
#define XPM_COLOR_ENABLE_RGB(color, mode)   ((color).mask |= 1 << 2 * (mode))
#define XPM_COLOR_ENABLE_HSV(color, mode)   ((color).mask |= 2 << 2 * (mode))
#define XPM_COLOR_ENABLE_NAME(color, mode)  ((color).mask |= 3 << 2 * (mode))
#define XPM_COLOR_GET_MEASURE(color, mode)  ((color).mask >> 2 * (mode) & 3)


/**
 * An RGB (red-green-blue) color value.
 */
typedef char xpm_color_rgb_t[3];

/**
 * An HSV (hue-saturation-value) color value.
 */
typedef char xpm_color_hsv_t[3];

/**
 * A color's value for some mode.
 */
typedef union xpm_colorvalue {
    xpm_color_rgb_t rgb;
    xpm_color_hsv_t hsv;
    char           *name;
} xpm_color_value_t;

/**
 * A color.  This can include values for all possible modes.
 */
typedef struct xpm_color {
    char             *name;
    int               mask;
    xpm_color_value_t values[XPM_COLORMODE_COUNT];
} xpm_color_t;

typedef enum xpm_color_mask {
    XPM_COLORMASK_NONE = 0,
    XPM_COLORMASK_RGB,
    XPM_COLORMASK_HSV,
    XPM_COLORMASK_NAME
} xpm_color_mask_t;


void xpm_color_print_value(FILE           *stream,
                           xpm_color_t     color,
                           xpm_colormode_t mode);


#endif
