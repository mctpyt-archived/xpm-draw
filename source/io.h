/***************************************************************************
 * Utility functions for input/output.                                     *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_IO_H
#define XPMDRAW_IO_H


#include <stdarg.h>
#include <stdio.h>


/**
 * String used as prefix for indenting some output lines.
 */
#define IO_INDENTATION  "    "


/**
 * Read a whole input stream.  Keep reading until EOF is found, storing the
 * results in a buffer that is dynamically allocated and is automatically
 * enlarged to accommodate its content.
 *
 * The parameter `stream` is the stream to read from.
 *
 * The return value is the buffer containing the contents streamed.  After
 * using it, it shall be manually destroyed by calling `free`.
 */
char *io_read_whole(FILE *stream);

/**
 * Print an error message to standard error.  The message given as argument
 * is `printf`-formatted, prefixed with `ERROR: ` (possibly colored) and
 * suffixed with `\n`.
 *
 * The parameter `format` is a `printf`-format string describing how to
 * format subsequent parameters for the message output.
 */
void io_print_error(const char *format,
                    ...);

/**
 * Print an error message to standard error.  The message given as argument
 * is `printf`-formatted, prefixed with `ERROR: ` (possibly colored) and
 * suffixed with `\n`.
 *
 * The parameter `format` is a `printf`-format string describing how to
 * format elements form the `list` parameter for the message output.  The
 * parameter `list` is an argument list that must be already-initialized (by
 * `va_start`).
 */
void io_print_error_v(const char *format,
                      va_list     list);

/**
 * Set error coordinates –file name, line number and column number– for the
 * next error message.  These coordinates refer to the location of an error
 * in an input file.  After printing the next error message, the coordinates
 * are cleared.
 *
 * The parameter `file_name` is a C string representing the name of the input
 * file.  The parameter `line` is the line number.  The parameter `column` is
 * the column number.
 */
void io_set_error_coords(const char *file_name,
                         unsigned    line,
                         unsigned    column);


#endif
