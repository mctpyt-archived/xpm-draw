/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "xpm.h"
#include "io.h"
#include "util.h"
#include "xpm_color.h"
#include "xpm_types.h"
#include <ctype.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


/***************************************************************************
 * Macros                                                                  *
 ***************************************************************************/

#define ERROR(...)                              \
    {                                           \
        if (!optional)                          \
            print_error_and_exit(__VA_ARGS__);  \
        return false;                           \
    }

#define OPTIONAL(expr)                             \
    {                                              \
        const bool _previous_optional = optional;  \
        optional           = true;                 \
        optional_succeeded = (expr);               \
        optional           = _previous_optional;   \
    }

#define MANDATORY(exp)                             \
    {                                              \
        const bool _previous_optional = optional;  \
        optional = false;                          \
        exp;                                       \
        optional = _previous_optional;             \
    }

#define PRINT_COLOR_VALUE(data, i, mode)  \
    xpm_color_print_value(stdout, (data)->colors[(i)], XPM_COLORMODE_##mode)


/***************************************************************************
 * Data                                                                    *
 ***************************************************************************/

/** XPM information obtained by the last parsing. */
static xpm_t       data;

/** Name of the current file. */
static const char *file_name;

/** Current file's raw content, represented as a C string. */
static char       *source;

/** Current position in the parsing of `source`, expressed as an offset. */
static size_t      cursor;

/** Whether the component being currently parsed is optional. */
static bool        optional;

/** Whether the previous optional component was successfully parsed. */
static bool        optional_succeeded;


/***************************************************************************
 * Private functions                                                       *
 ***************************************************************************/

static void print_error_and_exit(const char *message_format,
                                 ...)
{
    va_list            arg_list;
    util_cursor_info_t info;

    util_get_cursor_info(source, cursor, &info);
    io_set_error_coords(file_name, info.line, info.column);

    va_start(arg_list, message_format);
    io_print_error_v(message_format, arg_list);
    va_end(arg_list);

    util_draw_cursor_info(stderr, &info);

    exit(EXIT_FAILURE);
}

/* Component parsers. */

static bool parse_comment(void)
{
    if (source[cursor] != '/' || source[cursor + 1] != '*')
        ERROR("expected the string `/*`.");
    cursor += 2;

    MANDATORY({
        for (; source[cursor] != '*' || source[cursor + 1] != '/';
             cursor++)
            if (source[cursor] == '\0')
                ERROR("expected the string `*/` somewhere from here.");
        cursor += 2;
    })

    return true;
}

static bool parse_ws(void)
{
    switch (source[cursor])
    {
        case ' ':
        case '\t':
        case '\v':
        case '\n':
        case '\r':
        case '\f':
            cursor++;
            OPTIONAL(parse_ws());
            break;
        default:
            OPTIONAL(parse_comment());
            if (!optional_succeeded)
                ERROR("expected a whitespace character or the string `/*`.");
            OPTIONAL(parse_ws());
    }

    return true;
}

static bool parse_s(void)
{
    switch (source[cursor])
    {
        case ' ':
        case '\t':
            cursor++;
            OPTIONAL(parse_s());
            break;
        default:
            ERROR("expected space or tab.");
    }

    return true;
}

static bool parse_character(char character)
{
    if (source[cursor] != character)
        ERROR("expected the character `%c`.", character);

    cursor++;
    return true;
}

static bool parse_string(const char *string)
{
    const size_t string_length = strlen(string);

    if (strncmp(&source[cursor], string, string_length) != 0)
        ERROR("expected the string `%s`.", string);

    cursor += string_length;
    return true;
}

static bool parse_string_by_length(size_t length,
                                   char  *destination)
{
    size_t i;

    for (i = 0; i < length; i++) {
        if (source[cursor + i] == '\0')
            ERROR("expected %u more characters.", length - i);
        if (destination != NULL)
            destination[i] = source[cursor + i];
    }
    if (destination != NULL)
        destination[i] = '\0';

    cursor += i;
    return true;
}

static bool parse_string_from_list(const char *first,
                                   ...)
{
    va_list     arg_list;
    const char *string;

    va_start(arg_list, first);

    for (string = first;
         string != NULL;
         string = va_arg(arg_list, const char *)) {
        OPTIONAL(parse_string(string));
        if (optional_succeeded) {
            const char *matching_string, **destination;

            /* Traverse the remaining items in order to find the destination
               pointer.  Then put the matching string's address there. */
            matching_string = string;
            for (; string != NULL; string = va_arg(arg_list, const char *));
            destination = va_arg(arg_list, const char **);
            if (destination != NULL)
                *destination = matching_string;

            va_end(arg_list);
            return true;
        }
    }

    va_end(arg_list);
    ERROR("expected another string.");
}

static bool parse_string_from_dictlist(const char *first_key,
                                       int         first_value,
                                       ...)
{
    va_list     arg_list;
    const char *key;
    int         value;

    va_start(arg_list, first_value);

    for (key = first_key, value = first_value;
         key != NULL;
         key   = va_arg(arg_list, const char *),
         value = va_arg(arg_list, int)) {
        OPTIONAL(parse_string(key));
        if (optional_succeeded) {
            const int matching_value = value;
            int      *destination;

            /* Traverse the remaining items in order to find the destination
               pointer.  Then put the matching value there. */
            for (;; value = va_arg(arg_list, int)) {
                key = va_arg(arg_list, const char *);
                if (key == NULL)
                    break;
            }
            destination = va_arg(arg_list, int *);
            if (destination != NULL)
                *destination = matching_value;

            va_end(arg_list);
            return true;
        }
    }

    va_end(arg_list);
    ERROR("expected another string.");
}

static bool parse_string_until_character(char character)
{
    size_t own_cursor;

    for (own_cursor = cursor; source[own_cursor] != character; own_cursor++)
        if (source[own_cursor] == '\0')
            ERROR("expected the character `%c` somewhere from here.",
                  character);

    cursor = own_cursor + 1;
    return true;
}

static bool parse_decimal(unsigned *destination)
{
    const size_t previous_cursor = cursor;

    if (!isdigit(source[cursor]))
        ERROR("expected a decimal digit.");
    cursor++;

    for (; isdigit(source[cursor]); cursor++);
    if (destination != NULL)
        *destination = (unsigned) atoi(&source[previous_cursor]);

    return true;
}

static bool parse_hexdigits(unsigned digit_count,
                            char   **destination)
{
    unsigned i;

    for (i = 0; i < digit_count; i++) {
        unsigned x;

        if (!util_hexdigit_to_int(source[cursor + i], &x))
            ERROR("expected a hexadecimal digit.");
        if (destination != NULL)
            (*destination)[i / 2] |= x << 4 * (1 - i % 2);
    }

    cursor += i;
    return true;
}

static bool parse_rgb(xpm_color_rgb_t *destination)
{
    if (source[cursor] != '#')
        ERROR("expected the character `#`.");
    cursor++;

    parse_hexdigits(6, (char **) destination);
    return true;
}

static bool parse_hsv(xpm_color_hsv_t *destination)
{
    if (source[cursor] != '%')
        ERROR("expected the character `%`.");
    cursor++;

    parse_hexdigits(6, (char **) destination);
    return true;
}

static bool parse_id(void)
{
    if (!isalpha(source[cursor]) && source[cursor] != '_')
        ERROR("expected a letter or an underscore.");
    cursor++;

    for (; isalnum(source[cursor]) || source[cursor] == '_'; cursor++);

    return true;
}

/* Section parsers. */

static bool parse_start(void)
{
    parse_string("/* XPM */");
    return true;
}

static bool parse_declaration(void)
{
    OPTIONAL(parse_ws());
    parse_string("static");
    parse_ws();
    parse_string("char");
    OPTIONAL(parse_ws());
    parse_character('*');
    OPTIONAL(parse_ws());
    parse_id();
    OPTIONAL(parse_ws());
    parse_character('[');
    OPTIONAL(parse_ws());
    parse_character(']');
    OPTIONAL(parse_ws());
    parse_character('=');
    OPTIONAL(parse_ws());
    parse_character('{');

    return true;
}

static bool parse_header(bool *has_extensions)
{
    data.extension_count = 0;
    data.has_hotspot = *has_extensions = false;
    data.x_hotspot = data.y_hotspot = -1;
        /* Value to convey that the content is probably garbage (unless
           `width` or `height` is `MAX_UINT`. */

    OPTIONAL(parse_ws());
    parse_character('"');
    parse_decimal(&data.width);
    parse_s();
    parse_decimal(&data.height);
    parse_s();
    parse_decimal(&data.color_count);
    parse_s();
    parse_decimal(&data.characters_per_point);
    OPTIONAL(parse_s());
    if (optional_succeeded) {
        data.has_hotspot = true;
        parse_decimal(&data.x_hotspot);
        parse_s();
        parse_decimal(&data.y_hotspot);
    }
    OPTIONAL(parse_s());
    if (optional_succeeded) {
        parse_string("XPMEXT");
        *has_extensions = true;
    }
    parse_character('"');

    return true;
}

static bool parse_colors(void)
{
    const size_t size_for_colors = data.color_count * sizeof *data.colors;
    const size_t size_for_colornames = data.color_count
                                       * (data.characters_per_point + 1);
    unsigned i;

    data.colors = util_malloc(size_for_colors + size_for_colornames);
    for (i = 0; i < data.color_count; i++) {
        xpm_colormode_t mode;

        data.colors[i].name =
            &((char *) data.colors)[size_for_colors
                                    + i * (data.characters_per_point + 1)];
        XPM_COLOR_CLEAR(data.colors[i]);
        OPTIONAL(parse_ws());
        parse_character(',');
        OPTIONAL(parse_ws());
        parse_character('"');
        parse_string_by_length(data.characters_per_point,
                               data.colors[i].name);
        parse_s();
        parse_string_from_dictlist("m",  XPM_COLORMODE_M,
                                   "g4", XPM_COLORMODE_G4,
                                   "g",  XPM_COLORMODE_G,
                                   "c",  XPM_COLORMODE_C,
                                   "s",  XPM_COLORMODE_S,
                                   NULL, &mode);
        parse_s();
        OPTIONAL(parse_rgb(&data.colors[i].values[mode].rgb));
        if (optional_succeeded)
            XPM_COLOR_ENABLE_RGB(data.colors[i], mode);
        else {
            OPTIONAL(parse_hsv(&data.colors[i].values[mode].hsv));
            if (optional_succeeded)
                XPM_COLOR_ENABLE_HSV(data.colors[i], mode);
            else {
                parse_string_from_list("None", "black", "white",
                                       "red", "green", "blue",
                                       "yellow", "magenta", "cyan",
                                       NULL,
                                       &data.colors[i].values[mode].name);
                XPM_COLOR_ENABLE_NAME(data.colors[i], mode);
            }
        }
        parse_character('"');
    }

    return true;
}

static bool parse_points(void)
{
    unsigned i, j, k;

    data.points = util_malloc(data.height * data.width
                              * sizeof *data.points);
    for (i = 0; i < data.height; i++) {
        OPTIONAL(parse_ws());
        parse_character(',');
        OPTIONAL(parse_ws());
        parse_character('"');
        for (j = 0; j < data.width; j++) {
            char s[data.characters_per_point + 1];
            bool found;

            parse_string_by_length(data.characters_per_point, s);

            /* Look up the corresponding color. */
            found = false;
            for (k = 0; k < data.color_count; k++) {
                if (strcmp(data.colors[k].name, s) == 0) {
                    found = true;
                    data.points[i * data.width + j] = k;
                    break;
                }
            }
            if (!found) {
                cursor--;
                ERROR("invalid point.");
            }
        }
        parse_character('"');
    }

    return true;
}

static bool parse_extensions(bool has_extensions)
{
    for (;;) {
        OPTIONAL(parse_ws());
        OPTIONAL(parse_character(','));
        OPTIONAL(parse_ws());
        OPTIONAL(parse_string("\"XPMEXT"));
        if (!optional_succeeded) {
            if (has_extensions && data.extension_count == 0) {
                ERROR("expected at least one extension.");
            } else
                break;
        }
        data.extension_count++;
        parse_s();
        parse_id();
        OPTIONAL(parse_s());
        if (optional_succeeded)
            parse_string_until_character('"');
        else {
            parse_character('"');
            for (;;) {
                OPTIONAL(parse_ws());
                parse_character(',');
                OPTIONAL(parse_ws());
                parse_character('"');
                OPTIONAL(parse_string("XPMENDEXT\""));
                if (optional_succeeded)
                    break;
                parse_string_until_character('"');
            }
        }
    }

    return true;
}

static bool parse_end(void)
{
    OPTIONAL(parse_ws());
    OPTIONAL(parse_character(','));
    OPTIONAL(parse_ws());
    parse_string("}");
    OPTIONAL(parse_ws());
    parse_string(";");
    OPTIONAL(parse_ws());

    return true;
}


/***************************************************************************
 * Public functions                                                        *
 ***************************************************************************/

xpm_t *xpm_parse(const char *file_name_,
                 char       *source_)
{
    bool has_extensions;

    file_name = file_name_;
    source    = source_;

    parse_start();
    parse_declaration();
    parse_header(&has_extensions);
    parse_colors();
    parse_points();
    parse_extensions(has_extensions);
    parse_end();

    return &data;
}

void xpm_print_info(xpm_t *data)
{
    unsigned i;

    if (file_name == NULL)
        printf("XPM information:\n");
    else
        printf("XPM information about the file `%s`:\n", file_name);

    printf("* Width: %u points\n"
           "* Height: %u points\n"
           "* Color count: %u\n"
           "* Characters per point: %u\n",
           data->width, data->height, data->color_count,
           data->characters_per_point);

    if (data->has_hotspot)
        printf("* X hotspot: %u\n"
               "* Y hotspot: %u\n",
               data->x_hotspot, data->y_hotspot);
    else
        printf("* No hotspot\n");

    if (data->extension_count > 0)
        printf("* Extension count: %u\n", data->extension_count);
    else
        printf("* No extension\n");

    printf("* Colors:\n");
    for (i = 0; i < data->color_count; i++) {
        printf("  * `%s`: ", data->colors[i].name);
        PRINT_COLOR_VALUE(data, i, M);
        PRINT_COLOR_VALUE(data, i, G4);
        PRINT_COLOR_VALUE(data, i, G);
        PRINT_COLOR_VALUE(data, i, C);
        PRINT_COLOR_VALUE(data, i, S);
        printf("\n");
    }
}

void xpm_destroy(xpm_t *data)
{
    free(data->colors);
    free(data->points);
}
