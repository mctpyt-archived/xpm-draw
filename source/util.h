/***************************************************************************
 * Miscellaneous utilities.                                                *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XMPPARSER_UTIL_H
#define XMPPARSER_UTIL_H


#include <stdbool.h>
#include <stdio.h>


typedef struct util_cursor_info {
    const char *string;
    size_t      cursor;
    size_t      line;
    size_t      column;
    size_t      line_beginning;
    size_t      line_end;
} util_cursor_info_t;


bool util_hexdigit_to_int(char      hexdigit,
                          unsigned *destination);

void util_get_cursor_info(const char         *string,
                          size_t              cursor,
                          util_cursor_info_t *info_destination);

void util_draw_cursor_info(FILE                     *stream,
                           const util_cursor_info_t *info);

/**
 * Safety wrapper for `malloc`.  This function aborts in cases where `malloc`
 * returns `NULL`.
 *
 * See also: `util_calloc`, `util_realloc`.
 */
void *util_malloc(size_t size);

/**
 * Safety wrapper for `calloc`.  This function aborts in cases where `calloc`
 * returns `NULL`.
 *
 * See also: `util_malloc`, `util_realloc`.
 */
void *util_calloc(size_t element_count,
                  size_t element_size);

/**
 * Safety wrapper for `realloc`.  This function aborts in cases where
 * `realloc` returns `NULL`.
 *
 * See also: `util_malloc`, `util_calloc`.
 */
void *util_realloc(void  *pointer,
                   size_t size);


#endif
