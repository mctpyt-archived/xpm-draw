/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "xpm_draw.h"
#include "io.h"
#include "terminalcode.h"
#include "xpm_types.h"
#include <stdlib.h>
#include <string.h>


/***************************************************************************
 * Macro                                                                   *
 ***************************************************************************/

#define ERROR(...)  (io_print_error(__VA_ARGS__), exit(EXIT_FAILURE))


/***************************************************************************
 * Type                                                                    *
 ***************************************************************************/

typedef struct xpm_draw_map {
    const char *name;
    const char *terminalcode;
} xpm_draw_map_t;


/***************************************************************************
 * Private function                                                        *
 ***************************************************************************/

static const char *name_to_terminalcode(const char *name)
{
    static const xpm_draw_map_t map[] = {
        { "None",    TERMINALCODE_RESET      },
        { "black",   TERMINALCODE_BG_BLACK   },
        { "red",     TERMINALCODE_BG_RED     },
        { "green",   TERMINALCODE_BG_GREEN   },
        { "yellow",  TERMINALCODE_BG_YELLOW  },
        { "blue",    TERMINALCODE_BG_BLUE    },
        { "magenta", TERMINALCODE_BG_MAGENTA },
        { "cyan",    TERMINALCODE_BG_CYAN    },
        { "white",   TERMINALCODE_BG_WHITE   }
    };
    unsigned i;

    for (i = 0; i < sizeof map / sizeof *map; i++)
        if (strcmp(name, map[i].name) == 0)
            return map[i].terminalcode;

    ERROR("invalid color name!");
}


/***************************************************************************
 * Public function                                                         *
 ***************************************************************************/

void xpm_draw(xpm_t          *data,
              xpm_colormode_t mode)
{
    unsigned i, j;

    /* Autodetect mode.  Use the first mode found in the first color. */
    if (mode == XPM_COLORMODE_AUTODETECT)
        for (i = 0; i < XPM_COLORMODE_COUNT; i++)
            if (XPM_COLOR_GET_MEASURE(data->colors[0], i)
                  != XPM_COLORMASK_NONE) {
                mode = i;
                break;
            }

    /* Check whether all used colors are defined for this mode. */
    for (i = 0; i < data->color_count; i++)
        if (XPM_COLOR_GET_MEASURE(data->colors[i], mode)
              == XPM_COLORMASK_NONE)
            ERROR("the color `%s` has no value defined for the mode `%s`.",
                  data->colors[i].name, xpm_colormode_to_string(mode));

    /* Draw. */
    printf("\n");
    for (i = 0; i < data->height; i++) {
        for (j = 0; j < data->width; j++) {
            const xpm_point_t point = data->points[i * data->width + j];
            const char       *name  = data->colors[point].values[mode].name;
            const char       *terminalcode = name_to_terminalcode(name);

            printf(terminalcode);
            printf(" ");
        }
        printf(TERMINALCODE_RESET "\n");
    }
}
