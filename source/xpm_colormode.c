/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "xpm_colormode.h"
#include <string.h>


/***************************************************************************
 * Type                                                                    *
 ***************************************************************************/

typedef struct xpm_colormode_map {
    xpm_colormode_t mode;
    const char     *string;
} xpm_colormode_map_t;


/***************************************************************************
 * Data                                                                    *
 ***************************************************************************/

static const xpm_colormode_map_t map[] = {
    { XPM_COLORMODE_M,  "monochrome" },
    { XPM_COLORMODE_G4, "grey4"      },
    { XPM_COLORMODE_G,  "grey"       },
    { XPM_COLORMODE_C,  "color"      },
    { XPM_COLORMODE_S,  "symbolic"   }
};


/***************************************************************************
 * Public functions                                                        *
 ***************************************************************************/

xpm_colormode_t xpm_colormode_from_string(const char *string)
{
    unsigned i;

    for (i = 0; i < sizeof map / sizeof *map; i++)
        if (strcmp(string, map[i].string) == 0)
            return map[i].mode;

    return XPM_COLORMODE_INVALID;
}

const char *xpm_colormode_to_string(xpm_colormode_t mode)
{
    unsigned i;

    for (i = 0; i < sizeof map / sizeof *map; i++)
        if (mode == map[i].mode)
            return map[i].string;

    return "<invalid mode>";
}
