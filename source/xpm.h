/***************************************************************************
 * XPM parser.                                                             *
 ***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/
#ifndef XPMDRAW_XPM_H
#define XPMDRAW_XPM_H


#include "xpm_color.h"


/**
 * XPM information structure.
 */
typedef struct xpm xpm_t;


/**
 * Parse an XPM file.
 *
 * The parameter `file_name` is a C string to use as file name for error
 * messages.  The parameter `source` is a C string with the contents of an
 * XPM file.
 *
 * The return value is a structure containing information about the parsed
 * file.  After using it, it shall be manually destroyed by calling
 * `xpm_destroy`.
 */
xpm_t *xpm_parse(const char *file_name,
                 char       *source);

/**
 * Print information contained in an XPM information structure.
 *
 * The parameter `data` is an XPM information structure to use as input.
 */
void xpm_print_info(xpm_t *data);

/**
 * Destroy an XPM information structure.
 *
 * The parameter `data` is an XPM informatoin structure to destroy.
 */
void xpm_destroy(xpm_t *data);


#include "xpm_draw.h"


#endif
