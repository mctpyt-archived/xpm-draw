/***************************************************************************
 * Copyright © 2015 Mariano Street.                                        *
 * This file is part of XPM Draw.                                          *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation, either version 3 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License       *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 ***************************************************************************/


#include "config.h"
#include "fancy.h"
#include "io.h"
#include "xpm.h"
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>


/***************************************************************************
 * Macros                                                                  *
 ***************************************************************************/

#define ERROR(...)  (io_print_error(__VA_ARGS__), exit(EXIT_FAILURE))
#define VAR(s)      FANCY_ITALIC "<" s ">" FANCY_RESET


/***************************************************************************
 * Private functions                                                       *
 ***************************************************************************/

static void usage(const char *exec_name)
{
    printf("Usage: %s [" VAR("FILE NAME") " | " VAR("OPTION") "]\n"
           "       %s -- [" VAR("FILE NAME") "]\n",
           exec_name, exec_name);
    exit(EXIT_SUCCESS);
}

static void version(void)
{
    printf(
"XPM Draw %s\n"
"\n"
"Copyright © 2015 Mariano Street.\n"
"License GPLv3+: GNU GPL version 3 or later "
"<https://gnu.org/licenses/gpl.html>.\n"
"This is free software: you are welcome to change and redistribute it.\n"
"This program comes with ABSOLUTELY NO WARRANTY, to the extent permitted "
"by law.\n"
"\n"
"Source code repository: <https://gitorious.org/xpm-parser>.\n",
           CONFIG_VERSION_NUMBER);
    exit(EXIT_SUCCESS);
}

static char *read_file(const char *file_name)
{
    char *source;
    FILE *file;

    file = fopen(file_name, "r");
    if (file == NULL)
        ERROR("could not open file `" FANCY_ITALIC "%s" FANCY_RESET
              "`: %s.", file_name, strerror(errno));

    source = io_read_whole(file);
    fclose(file);
    return source;
}

static void parse_command_line(int          argc,
                               char        *argv[],
                               bool        *will_print_info_destination,
                               bool        *will_draw_destination,
                               const char **mode_string_destination)
{
    const struct option long_options[] = {
        { "all",     no_argument,       0, 'a' },
        { "info",    no_argument,       0, 'i' },
        { "mode",    required_argument, 0, 'm' },
        { "help",    no_argument,       0, 'h' },
        { "version", no_argument,       0, 'V' },
        { 0,         0,                 0, 0   }
    };
    bool a_enabled, i_enabled, m_enabled;
    int  c;

    opterr = 0;
    a_enabled = false;
    i_enabled = false;
    m_enabled = false;
    while ((c = getopt_long(argc, argv, "ahim:V", long_options, NULL)) != -1)
        switch (c) {
            case 'a':
                if (i_enabled)
                    ERROR("cannot set option `-a`/`--all` since "
                          "`-i`/`--info` has already been set.");
                if (m_enabled)
                    ERROR("cannot set option `-i`/`--info` since "
                          "`-m`/`--mode` has already been set.");
                a_enabled                    = true;
                *will_print_info_destination = true;
                break;
            case 'i':
                if (a_enabled)
                    ERROR("cannot set option `-i`/`--info` since "
                          "`-a`/`--all` has already been set.");
                if (m_enabled)
                    ERROR("cannot set option `-i`/`--info` since "
                          "`-m`/`--mode` has already been set.");
                i_enabled                    = true;
                *will_print_info_destination = true;
                *will_draw_destination       = false;
                break;
            case 'h':
                usage(argv[0]);
            case 'm':
                if (a_enabled)
                    ERROR("cannot set option `-m`/`--mode` since "
                          "`-a`/`--all` has already been set.");
                if (i_enabled)
                    ERROR("cannot set option `-m`/`--mode` since "
                          "`-i`/`--info` has already been set.");
                m_enabled                = true;
                *mode_string_destination = optarg;
                break;
            case 'V':
                version();
            case '?':
                ERROR("unknown option `%s`.", argv[optind - 1]);
        }
}


/***************************************************************************
 * Public function                                                         *
 ***************************************************************************/

int main(int   argc,
         char *argv[])
{
    bool            will_print_info, will_draw;
    xpm_t          *data;
    xpm_colormode_t mode;
    char           *source;
    const char     *file_name, *mode_string;

    will_print_info = false;
    will_draw       = true;
    mode_string     = NULL;
    parse_command_line(argc, argv,
                       &will_print_info, &will_draw, &mode_string);

    /* Prepare XPM input. */

    if (mode_string == NULL)
        mode = XPM_COLORMODE_AUTODETECT;
    else {
        mode = xpm_colormode_from_string(mode_string);
        if (mode == XPM_COLORMODE_INVALID)
            ERROR("invalid color mode `%s`.", mode_string);
    }

    if (optind == argc) {
        file_name = NULL;
        source    = io_read_whole(stdin);
    } else if (optind == argc - 1) {
        file_name = argv[optind];
        source    = read_file(file_name);
    } else
        ERROR("too many arguments");

    /* Parse XPM and output as requested. */

    data = xpm_parse(file_name, source);

    if (will_print_info)
        xpm_print_info(data);

    if (will_draw)
        xpm_draw(data, mode);

    /* Free resources. */

    xpm_destroy(data);
    free(source);

    return EXIT_SUCCESS;
}
